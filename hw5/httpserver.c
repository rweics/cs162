/* A simple HTTP server with port number is passed as an argument */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/stat.h>
 
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <dirent.h>

#define MAXPATH 1024
#define MAXBUF 10240
#define MAXREQ  8192
#define MAXQUEUE 5

void error(char *msg)
{
    perror(msg);
    exit(1);
}

const char dir_listing_head[] = 
"<html>"
"<head>"
"<title>index of %s </title>"
"</head>"
"<body>"
"<h1> Index of %s</h1>"
"<table>"
"<tr> <th style='width:300px'> Name </th> <th style='width:300px'> Type </tr>"; // %s refers to the directory name

const char dir_file_entry[] = 
"<tr> <td>%s</td> <td>Regular File</td> </tr>"; // %s refers to a file name within the directory
const char dir_dir_entry[] = 
"<tr> <td>%s</td> <td>Directory</td> </tr>"; // %s refers to a sub-directory name within the directory
const char dir_listing_end[] = 
"</table> </body> </html>\r\n\r\n";

const char htmlheader[]="HTTP/1.0 200 OK\r\n"
  "Content-Type: text/html\r\n"
  "\r\n";

const char textheader[]="HTTP/1.0 200 OK\r\n"
  "Content-Type: text/plain\r\n"
  "\r\n";

const char badreqheader[]="HTTP/1.0 400 Bad Request\r\n"
  "Content-Type: text/html\r\n"
  "\r\n";
  
const char notfoundheader[]="HTTP/1.0 404 Not Found\r\n"
  "Content-Type: text/html\r\n"
  "\r\n"; 

const char forbidden[]="HTTP/1.0 403 Forbidden\r\n"
  "Content-Type: text/html\r\n"
  "\r\n"; 

int process_http_request(int httpsockfd)
{
  char reqbuf[MAXREQ];
  int n=0;
  /* Note this is same as the HW2 skeleton.
   * Replace this with your HW2 implementation
   * */
  memset(reqbuf,0, MAXREQ);
  n = read(httpsockfd,reqbuf,MAXREQ-1);
  int current_char;
  char *request = strstr(reqbuf, "GET");
  char buf[MAXBUF];
  char *buf_current = buf;
  if (!request) {
    /* if the request is not GET */
    write(httpsockfd,badreqheader,strlen(badreqheader));
    FILE* content = fopen("400.html", "r");
    while ((current_char = fgetc(content)) != EOF) {
      *buf_current = current_char;
      buf_current++;
    }
    *buf_current = '\0';
    write(httpsockfd,buf,strlen(buf));
    fclose(content);
  } else {
    char *req_end = strstr(request+4, " ");
    *req_end = '\0';
    int escaped = 0;
    char *p = request;
    while (*p != '\0') {
      if (*p == '.' && *(p+1) == '.') escaped = 1;
      printf("%c", *p);
      p++;
    }
    char filename[MAXPATH];
    strcpy(filename, "./www");
    int path_length = req_end-(request+4);
    strncpy(filename+5, request+4, path_length);
    filename[path_length+5] = '\0';
    FILE* content = fopen(filename, "r");
    if (content) {
      /* if the file exists */
      int fd = open(filename, O_RDONLY);
      struct stat stat;
      fstat(fd, &stat);
      if (!(S_IROTH & stat.st_mode) || escaped) {
        /* if bad permission */
        write(httpsockfd,forbidden,strlen(forbidden));
        FILE* f = fopen("403.html", "r");
        while ((current_char = fgetc(f)) != EOF) {
          *buf_current = current_char;
          buf_current++;
        }
        *buf_current = '\0';
        write(httpsockfd,buf,strlen(buf));
        fclose(f);
      } else {
        write(httpsockfd,htmlheader,strlen(htmlheader));
        if (S_ISDIR(stat.st_mode)) {
          DIR *directory;
          struct dirent *dir;
          directory = opendir(filename);
          if (*(filename+4+path_length) == '/')
            strcpy(filename+5+path_length, "index.html");
          else
            strcpy(filename+5+path_length, "/index.html");
          FILE* indexhtml = fopen(filename, "r");
          if (indexhtml) {
            while ((current_char = fgetc(indexhtml)) != EOF) {
              *buf_current = current_char;
              buf_current++;
            }
            *buf_current = '\0';
            write(httpsockfd,buf,strlen(buf));
            fclose(indexhtml);
          } else {
            char directory_header[MAXBUF];
            sprintf(directory_header, dir_listing_head, request+4, request+4);
            write(httpsockfd,directory_header,strlen(directory_header));
            /* Consulted http://stackoverflow.com/questions/4204666/how-to-list-files-in-a-directory-in-a-c-program */
            if (directory) {
              while ((dir = readdir(directory)) != NULL) {
                char directory_item[MAXBUF];
                if (dir->d_type == DT_DIR)
                  sprintf(directory_item, dir_dir_entry, dir->d_name);
                else sprintf(directory_item, dir_file_entry, dir->d_name);
                write(httpsockfd,directory_item,strlen(directory_item));
              }
              write(httpsockfd,dir_listing_end,strlen(dir_listing_end));
            }
          }
          closedir(directory);
        } else {
          while ((current_char = fgetc(content)) != EOF) {
            *buf_current = current_char;
            buf_current++;
          }
          *buf_current = '\0';
          write(httpsockfd,buf,strlen(buf));
        }
      }
      close(fd);
    } else {
      /* if the file does not exist */
      write(httpsockfd,notfoundheader,strlen(notfoundheader));
      content = fopen("404.html", "r");
      while ((current_char = fgetc(content)) != EOF) {
        *buf_current = current_char;
        buf_current++;
      }
      *buf_current = '\0';
      write(httpsockfd,buf,strlen(buf));
    }
    fclose(content);
  }
  write(STDOUT_FILENO, reqbuf, n );
  return 0;
}

int sockfd, newsockfd;    /* make static so signal handler can close */

void signal_callback_handler(int signum)
{
  printf("Caught signal %d\n",signum);
  printf("Close socket %d\n", sockfd);
  if (close(sockfd) < 0) perror("failed to close sockfd\n");
  exit(signum);
}

int server(int portno)
{
  struct sockaddr_in serv_addr; /* internet style socket address object */
  struct sockaddr_in cli_addr;
  uint clilen = sizeof(cli_addr);

  pid_t cpid;
  int socketOption = 1;

  signal(SIGINT, signal_callback_handler);

  /* Create Socket to receive requests*/
  sockfd = socket(PF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) error("ERROR opening socket");
  printf("Got socket: %d\n", sockfd);

  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &socketOption, sizeof(socketOption)))
    error("ERROR setting reuseadd option");

  /* Bind socket to port */
  memset((char *) &serv_addr,0,sizeof(serv_addr));
  serv_addr.sin_family      = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port        = htons(portno);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    close(sockfd);
    error("ERROR on binding");
  }

  while (1) {
    listen(sockfd,MAXQUEUE);    /* Listen for incoming connections */

    /* Accept incoming connection, obtaining a new socket for it */
    if ((newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen)) < 0)
      error("ERROR on accept");
    printf("new socket: %d\n", newsockfd);

    cpid = fork();         /* create new process for connection */
    if (cpid > 0) {     /* parent process */
      close(newsockfd);     /* parent drops refernce on connection socket */
      printf("Ready for next\n");
    } else if (cpid == 0) {   /* child process */
      close(sockfd);      /* clild drops reference on listening socket */
      process_http_request(newsockfd);
      close(newsockfd); /* child drops refernce on connection socket */
      exit(EXIT_SUCCESS); /* exit child normally */
    } else {      /* fork failed */
      error("Fork of child process failed\n");
    }
  }
  printf("Server exiting\n");
  close(sockfd);
  return 0; 
}


int main(int argc, char *argv[])
{
  int portno;
  if (argc < 2) {
    fprintf(stderr,"usage %s portno\n", argv[0]);
    exit(1);
  }
  portno = atoi(argv[1]);
  printf("Opening server on port %d\n",portno);
  return server(portno);
}
