#include <stdio.h>
#include <ctype.h>

void wc(FILE *ofile, FILE *infile, char *inname) {
    int char_count = 0;
    int word_count = 0;
    int line_count = 0;
    char current_char;
    char prev_char = '\n';
    while ((current_char = fgetc(infile)) != EOF) {
        char_count += 1;
        if (current_char == '\n')
            line_count += 1;
        if (!isspace(current_char) && isspace(prev_char))
            word_count += 1;
        prev_char = current_char;
    }
    printf("%4d %4d %4d %s\n", line_count, word_count, char_count, inname);
    if (ofile)
        fprintf(ofile, "%4d %4d %4d %s\n", line_count, word_count, char_count, inname);
}

int main (int argc, char *argv[]) {
    FILE* infile = fopen(argv[1], "r");
    FILE* ofile = NULL;
    if (argc == 3) ofile = fopen(argv[2], "w");
    wc(ofile, infile, argv[1]);
    fclose(infile);
    return 0;
}
