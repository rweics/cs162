#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define FALSE 0
#define TRUE 1
#include "io.h"
#include "parse.h"

int cmd_quit(tok_t arg[]) {
  printf("Bye\n");
  exit(0);
  return 1;
}

int cmd_help(tok_t arg[]);

int cmd_cd(tok_t arg[]){
  return chdir(arg[0]);
}

int cmd_wait(pid_t cpid, int *status) {
  pid_t w = waitpid(cpid, status, WNOHANG|WUNTRACED);
  if (w < 0) {
    perror("background failed");
    exit(EXIT_FAILURE);
  } else if (w == cpid)  // child ended
    exit(EXIT_SUCCESS);
  return 1;
}

/* Command Lookup table */
typedef int cmd_fun_t (tok_t args[]); /* cmd functions take token arra and rtn int */
typedef struct fun_desc {
  cmd_fun_t *fun;
  char *cmd;
  char *doc;
} fun_desc_t;

fun_desc_t cmd_table[] = {
  {cmd_help, "?", "show this help menu"},
  {cmd_quit, "quit", "quit the command shell"},
  {cmd_cd, "cd", "change current directory"}
};

int cmd_help(tok_t arg[]) {
  int i;
  for (i=0; i < (sizeof(cmd_table)/sizeof(fun_desc_t)); i++) {
    printf("%s - %s\n",cmd_table[i].cmd, cmd_table[i].doc);
  }
  return 1;
}

int lookup(char cmd[]) {
  int i;
  for (i=0; i < (sizeof(cmd_table)/sizeof(fun_desc_t)); i++) {
    if (strcmp(cmd_table[i].cmd, cmd) == 0) return i;
  }
  return -1;
}

int shell (int argc, char *argv[]) {
  char *s;      /* user input string */
  tok_t *t;     /* tokens parsed from input */
  int lineNum = 0;
  int fundex = -1;
  pid_t pid = getpid();   /* get current processes PID */
  pid_t ppid = getppid(); /* get parents PID */

  printf("%s running as PID %d under %d\n",argv[0],pid,ppid);

  lineNum=0;
  fprintf(stdout,"%d: ",lineNum);
  fprintf(stdout,"%s: ", getcwd(NULL, 64));
  while ((s = freadln(stdin))) {
    t = getToks(s);   /* Break the line into tokens */
    fundex = lookup(t[0]);  /* Is first token a shell literal */
    if (fundex >= 0) cmd_table[fundex].fun(&t[1]);
    else {      /* Treat it as a file to exec */
      pid_t forkpid = fork();
      int status;
      int backGround = FALSE;
      int offset = 0;
      while (*(t+offset)){
        if ((*(t+offset))[strlen(*(t+offset))-1] == '&'){
          backGround = TRUE;
          (*(t+offset))[strlen(*(t+offset))-1] = FALSE;
        }
        offset += 1;
      }
      if (forkpid == 0) {
        status = execv(*t, t);
        extern char **environ;
        const char *paths = getenv("PATH");
        char *a_path = strchr(paths, 58);
        while (a_path) {
          a_path = strchr(paths, 58);
          if (a_path){
            *a_path = 0;
          }
          char *current_path = strcat(strcat(strdup(paths), "/"), *t);
          status = execve(current_path, t, environ);
          paths = a_path;
          paths++;
        }
        exit(EXIT_SUCCESS);
      } else if (forkpid > 0) {
        if (backGround) {
          backGround = FALSE;
          cmd_wait(forkpid, &status);
        }
        wait(&status);
      } else {
        perror("Fork failed");
        exit(1);
      }
    }
    fprintf(stdout,"%d: ",++lineNum);
    fprintf(stdout,"%s: ", getcwd(NULL, 64));
  }
  return 0;
}

int main (int argc, char *argv[]) {
  return shell(argc, argv);
}
