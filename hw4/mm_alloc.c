/*
 * mm_alloc.c
 *
 * Stub implementations of the mm_* routines. Remove this comment and provide
 * a summary of your allocator's design here.
 */

#include "mm_alloc.h"

#include <stdlib.h>
#include <unistd.h>

/* Your final implementation should comment out this macro. */
// #define MM_USE_STUBS

s_block_ptr meta_data = NULL;
s_block_ptr heap_end;

void* mm_malloc(size_t size)
{
#ifdef MM_USE_STUBS
    return calloc(1, size);
#else
    if (!meta_data) {
    	meta_data = sbrk(BLOCK_SIZE);
    	meta_data->prev = NULL;
    	meta_data->next = NULL;
    	meta_data->size = size;
    	meta_data->free = 0;
    	meta_data->ptr = sbrk(size);
    	heap_end = meta_data;
    	return meta_data->ptr;
    } else {
    	s_block_ptr block = meta_data;
    	while (block->next) {
    		if (block->free) {
    			if (block->size >= size) {
    				split_block(block, size);
    				block->free = 0;
    				return block->ptr;
    			} else {
    				s_block_ptr temp = fusion(block);
    				if (temp->size >= size) {
    					split_block(temp, size);
    					block->free = 0;
    					return temp->ptr;
    				}
    			}
    		}
    		block = block->next;
    	}
    	block = extend_heap(block, size);
    	if (block != NULL) {
    		block->free = 0;
    		return block->ptr;
    	}
    	else return NULL;
    }
#endif
}

void* mm_realloc(void* ptr, size_t size)
{
#ifdef MM_USE_STUBS
    return realloc(ptr, size);
#else
    s_block_ptr block = get_block(ptr);
    if (block->free && block->size >= size) {
    	split_block(block, size);
    	block->free = 0;
    	return block->ptr;
    } else {
    	return mm_malloc(size);
    }
#endif
}

void mm_free(void* ptr)
{
#ifdef MM_USE_STUBS
    free(ptr);
#else
    s_block_ptr block = get_block(ptr);
    if (block != NULL) {
    	if (block == heap_end) {
    		sbrk(-1*block->size);
    		block->prev->next = NULL;
    		heap_end = block->prev;
    		sbrk(-1*BLOCK_SIZE);
    	} else {
    		block->free = 1;
    	}
    }
#endif
}

// /* Split block according to size, b must exist */
void split_block (s_block_ptr b, size_t s) {
	size_t new_block_size = b->size - s;
	if (new_block_size > 0) {
		b->size = s;
		s_block_ptr new_block = (s_block_ptr) mm_malloc(BLOCK_SIZE);
		new_block->prev = b;
		new_block->next = b->next;
		b->next = new_block;
		new_block->size = new_block_size;
		new_block->ptr = b->ptr + s;
		new_block->free = 1;
	}
}

// /* Try fusing block with neighbors */
s_block_ptr fusion(s_block_ptr b) {
	if (!b->next || !b->next->free)
		return b;
	else {
		s_block_ptr block_to_remove = b->next;
		b->next = block_to_remove->next;
		if (block_to_remove->next) block_to_remove->next->prev = b;
		b->size += block_to_remove->size;
		mm_free(block_to_remove);
		return fusion(b);
	}
}

/* Get the block from addr */ 
s_block_ptr get_block (void *p) {
	s_block_ptr block = meta_data;
	while (block->next) {
		if (block->ptr <= p && block->ptr + block->size > p) return block;
		block = block->next;
	}
	return NULL;
}

/* Add a new block at the of heap,
 * return NULL if things go wrong
 */
s_block_ptr extend_heap (s_block_ptr last , size_t s) {
	// need to check if it is still possible to allocate more
	s_block_ptr new_block = (s_block_ptr) sbrk(BLOCK_SIZE);
	new_block->prev = last;
	last->next = new_block;
	new_block->free = 1;
	new_block->size = s;
	new_block->ptr = sbrk(s);
	heap_end = new_block;
	return new_block;
}